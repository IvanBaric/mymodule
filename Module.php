<?php

namespace MyModule;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Cache\StorageFactory;

class Module
{
	#public $em;

	public function onBootstrap(MvcEvent $e) {
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
		
		
		// Translate ZendValidate - Croatian
		\MyModule\Service\TranslateZendValidate::translate();


		// http://stackoverflow.com/questions/12562538/zf2-creation-of-simple-service-and-access-it-through-viewhelper
		#$sm = $e->getApplication()->getServiceManager();
		#$this->em = $sm->get('Doctrine\ORM\EntityManager');


	}
	
	
	
	public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
	
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
    	return array(
    			'abstract_factories' => array(),
    			'aliases' => array(),
    			'factories' => array(
    					
//    				'Cache' => function($sm){
//
//    					$config = $sm->get('Config');
//
//    					$setup = array(
//    							'adapter' => $config['cache']['adapter'],
//    							'options' => array(
//    									'ttl' => $config['cache']['ttl'],
//    							),
//    							'plugins' => array(
//    									'exception_handler' => array(
//    											'throw_exceptions' => TRUE
//    									),
//    									'Serializer'
//    							)
//    					);
//
//    					return $cache = StorageFactory::factory($setup);
//
//    				},
    				'CommonFieldset' => function($sm){
    					$form = new \MyModule\Form\CommonFieldset();
    					return $form;
    				},
    				
    				'CheckboxForm' => function($sm){
    					$form = new \MyModule\Form\CheckboxForm();
    					return $form;
    				},
    				
    				'MyCrud' => function($sm){
    					$model = new \MyModule\Crud\Crud($sm);
    					#$model->setServiceLocator($sm);
    					return $model;
    				},

					'MyRepository' => function($sm){
						$model = new \MyModule\Repository\MyRepository($sm);
						#$model->setServiceLocator($sm);
						return $model;
					},
    				
    				'SortParamsForm' => function($sm){
    					$form = new \MyModule\Form\SortParamsForm();
    					$form->setInputFilter($sm->get('SortParamsFilter'));
    					return $form;
    				},
    					
    				'SortParamsFilter' => function($sm){
    					$filter = new \MyModule\Form\SortParamsFilter();
    					return $filter;
    				},
    				

    				'SimpleBatchInsertForm' => function($sm){
    					$form = new \MyModule\Form\BatchInsert\SimpleBatchInsertForm();
    					$form->setInputFilter($sm->get('SimpleBatchInsertFilter'));
    					return $form;
    				},
    				
    				'SimpleBatchInsertFilter' => function($sm){
    					return new \MyModule\Form\BatchInsert\SimpleBatchInsertFilter();
    				},
    					
    			),
    			'invokables' => array(),
    			'services' => array(),
    			'shared' => array(),
    	);
    }
    
    public function getViewHelperConfig(){
    	return array(
    			'invokables' => array(
    					'alert' => 'MyModule\View\Helper\Alert',
						'flashMessages'	=> 'MyModule\View\Helper\FlashMessages',
    					'label' => 'MyModule\View\Helper\Label',
    					'sort'	=> 'MyModule\View\Helper\Sort\Sort',
    					'sortConfig'	=> 'MyModule\View\Helper\Sort\SortConfig',
    					'checkFetchError'	=> 'MyModule\View\Helper\Table\CheckFetchError',
    					'tableOpenTag'	=> 'MyModule\View\Helper\Table\TableOpenTag',
    					'tableCloseTag'	=> 'MyModule\View\Helper\Table\TableCloseTag',
    					'editUrl'	=> 'MyModule\View\Helper\Url\EditUrl',
    					'deleteUrl'	=> 'MyModule\View\Helper\Url\DeleteUrl',
    					'widgetStats'	=> 'MyModule\View\Helper\Widget\WidgetStats',
    					'CalculatePercent'	=> 'MyModule\View\Helper\Chart\CalculatePercent',
    					'actionButtons'	=> 'Application\View\Helper\ActionButtons',
    					'editBtn'	=> 'MyModule\View\Helper\Button\EditBtn',
    					'deleteBtn'	=> 'MyModule\View\Helper\Button\DeleteBtn',
						'confirmDeleteBtn'	=> 'MyModule\View\Helper\Button\ConfirmDeleteBtn',
    					'boxGeneric'	=> 'MyModule\View\Helper\Box\BoxGeneric',
    					'entityDetails'	=> 'MyModule\View\Helper\Entity\EntityDetails',
						'isFeatured'	=> 'MyModule\View\Helper\IsFeatured',
						'isVisible'	=> 'MyModule\View\Helper\IsVisible',
						'price'	=> 'MyModule\View\Helper\Price',
						'discount'	=> 'MyModule\View\Helper\Discount',
						'base'	=> 'MyModule\View\Helper\Base',
						'permission'	=> 'MyModule\View\Helper\Permission\Permission',
						'edit'	=> 'MyModule\View\Helper\Action\Edit',
						'delete'	=> 'MyModule\View\Helper\Action\Delete',
						'read'	=> 'MyModule\View\Helper\Action\Read',


    					#'formelementerrors' => 'MyModule\View\Helper\FormElementErrors',
    					#'formelement' => 'MyModule\View\Helper\FormElement',
    					#'formcollection' => 'MyModule\View\Helper\FormCollection',
    					
    					
    			),
				'factories' => array(
						'config' => function($sm) {
							$helper = new \MyModule\View\Helper\Config($sm);
							return $helper;
						},
						'hash' => function() {
							$helper = new \MyModule\View\Helper\Hashids\Hash();
							return $helper;
						},

						'em' => function ($sm) {
							$helper = new \MyModule\View\Helper\EntityManager\Em($sm);
							return $helper;
						},

				)
		);
    }
}