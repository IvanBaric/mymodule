<?php
namespace MyModule\Settings;

class MySettings {

    public function __construct(){
        $this->setRedirect();
        $this->setDefaultRouteSettings();
    }

	//has Orphans
	public $hasOrphans = true;
	public $hasOrphansClass = "";
	public $hasOrphansProperty = "name";

	// provjerava postoji li unos već u bazi podataka
	public $recordExists = false;

	public $afterDeleteRedirectToPreviousPage = FALSE;


	// Controller Settings
	public $message = array(
			'insert' => array(
					'success' => 'Uspješno uneseno u bazu podataka.',
					'error' => 'Dogodila se pogreška prilikom unosa u bazu podataka.',
					'recordExists' => 'Unos postoji u bazi podataka',
					'readOnly' => 'Nemate dopuštenje za unos novog zapisa u bazu podataka.'
			),

			'edit' => array(
					'success' => 'Podaci su promijenjeni',
					'error' => 'Dogodila se pogreška prilikom uređivanja podataka iz baze.',
					'readOnly' => 'Ne postoji mogućnost uređivanja zapisa'
			),

			'delete' => array(
					'success' => 'Izbrisano iz baze podataka.',
					'error'	  => 'Dogodila se pogreška prilikom brisanja iz baze podataka.',
					'notUserEntity' => "Nemate dopuštenje za brisanje unosa iz baze podataka.",
					'deleteOrphans' => "Unos se ne može izbrisati jer se koristi u drugoj tablici.",
					'readOnly' => 'Unos se ne može brisati iz baze podataka',
					'softDeleteError' => 'Ne može se izvršiti soft delete',
					'softDelete' => 'Zapis je premješten u smeće.'
			),
			'fetch' => array(
					'errorId' => 'Id nije valjan',
					'recordNotExists' => 'Traženi unos ne postoji',
					'emptyResult' => 'Nema rezultata',
					'accessDenied' => 'Nemate pristup traženom podatku.'
			),
	);






/**************************************************************************************************************
 * SortConfig
 **************************************************************************************************************/
	public $defaultTableOrderByColumn = 'id';
	public $defaultTableOrder = 'DESC';
	public $defaultItemsPerPage = 5;
	public $defaultPageRange = 5;

	//Controller-Model Settings
	public $class = array(
			'form' => '',
			'model' => '',
			'entity' => ''
	);

	// View Settings
	public $submitButtonText = array(
			'insert' => 'Unesi',
			'edit' => 'Spremi promjene'
	);

	public $pageHeading = array(
			'insert' => "Add new",
			'edit' => "Edit"
	);


/**************************************************************************************************************
 * RedirectSettings
 **************************************************************************************************************/
	public $redirect = array(
			'insert' => array(
					'success' => '',
					'error' => ''
			),

			'edit' => array(
					'success' => '',
					'error' => ''
			),

			'delete' => array(
					'success' => '',
					'error' => ''
			),
	);

	public function setRedirect(){
		foreach ($this->redirect as $key => $value){
            foreach($value as $type => $route){
                $this->redirect[$key][$type] = $this->entityRoute;
            }
		}

	}

/**************************************************************************************************************
 * RouteSettings
 **************************************************************************************************************/
	public $entityRoute = "";

	public $route = array(
			'base' => '',
			'default' => 'default',
			'ajaxSearch' => 'ajaxSearch',
//			'details' => 'details',
			'edit' => 'edit',
			'delete' => 'delete',
			'confirmDelete' => 'confirmDelete',
			'details' => 'details'

	);

	public function setDefaultRouteSettings(){
		foreach ($this->route as $key => $value){
			$this->route[$key] = ($key == 'base') ?  $this->entityRoute : $this->entityRoute . "/". $value;
		}
	}



} // MySettings





