<?php
namespace MyModule\Service;

use Zend\Cache\StorageFactory;
class Cache {
	
	public static function factory($key, $value){
		
		$config = array(
			'adapter' => 'apc',
			'options' => array(
				'ttl' => 10
			),
			'plugins' => array(
				'exception_handler' => array(
					'throw_exceptions' => FALSE
				),
					'Serializer'
			)
		);
		
		$cache = StorageFactory::factory($config);
		
		
		if(!$result = $cache->getItem($key) ){
			echo "nema cache <br />";
			$result = $cache->addItem($key, $value);
		}
		
		echo $result;
	}
}