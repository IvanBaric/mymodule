<?php
namespace MyModule\Service;

class TranslateZendValidate {
	
	public static function translate(){
		$translatorI = new \Zend\I18n\Translator\Translator();
		$translatorI->setLocale('hr_HR');



//		$translator = new \Zend\Mvc\I18n\Translator();

		$translator = new \Zend\Mvc\I18n\Translator($translatorI);
		
		$translator->addTranslationFile(
				'phpArray',
//				'resources/languages/hr/Zend_Validate.php', //or Zend_Captcha
//				'./vendor/zendframework/zendframework/resources/languages/hr/Zend_Validate.php',
//				'./data/resources/languages/hr/Zend_Validate.php',
				'./module/MyModule/data/resources/languages/hr/Zend_Validate.php',
				'default',
				'hr_HR'
		);
		
		return \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
	}

}