<?php

//*************************************************
//P E R M I S S I O N    C L A S S
//*************************************************
//
//*DENY*
//- CREATE - C (CONTROLLER ACTION, VIEW 'ADD NEW' BUTTON)
//- UPDATE - U (CONTROLLER ACTION, VIEW 'EDIT' BUTTON)
//- DELETE - D (CONTROLLER ACTION, VIEW 'DELETE' BUTTON)
//- READ   - R (CREATE, UPDATE, DELETE) (CONTROLLER ACTION, VIEW 'ADD NEW', 'EDIT', 'DELETE' BUTTON)
//
//
//*ALLOW*
//- UPDATE ONLY USER ENTITIES  - UUE  (repository)
//- DELETE ONLY USER ENTITIES  - DUE  (repository, )
//- READ ONLY USER ENTITIES    - RUE

// *REQUIRE* SOFT DELETE
// http://stackoverflow.com/questions/5020568/soft-delete-best-practices-php-mysql
//0 -> Not Deleted
//1 -> Soft Deleted, shows up in lists of deleted items for management users
//2 -> Soft Deleted, does not show up for any user except admin users
//3 -> Only shows up for developers.


namespace MyModule\Permission;

class MyPermission {

    private $permission = array();


// DENY
    const CREATE = "C";
    const READ   = "R";
    const UPDATE = "U";
    const DELETE = "D";

// ALLOW
    const UPDATE_USER_ENTITY = "UUE";
    const DELETE_USER_ENTITY = "DUE";
    const READ_USER_ENTITY   = "RUE";

// REQUIRE
    const SOFT_DELETE = "SOFT_DELETE";




    public function __construct($user) {
        $this->setUserId($user->getId());
        $this->setRole($user->getRole()->getAbbrv());
    }

    

    
    protected function deny($role, $permission){
        $this->permission['deny'][$role] = $permission;
    }

    protected function allow($role, $permission){
        $this->permission['allow'][$role] = $permission;
    }

    protected function requireAction($role, $action){
        $this->permission['require'][$role] = $action;
    }

    public function isAllowed($action){
        if(in_array($action, array(self::CREATE, self::READ, self::UPDATE, self::DELETE))) {
            $permissionType = "deny";

            if (!array_key_exists($permissionType, $this->permission)) {
                return true;
            }

            if (!array_key_exists($this->getRole(), $this->permission[$permissionType])) {
                return true;
            }

            if (!in_array($action, $this->permission[$permissionType][$this->getRole()])) {
                return true;
            }

            return false;
        }

        if(in_array($action, array(self::UPDATE_USER_ENTITY, self::DELETE_USER_ENTITY, self::READ_USER_ENTITY))) {
            $permissionType = "allow";

            if (!array_key_exists($permissionType, $this->permission)) {
                return false;
            }

            if (!array_key_exists($this->getRole(), $this->permission[$permissionType])) {
                return false;
            }

            if (!in_array($action, $this->permission[$permissionType][$this->getRole()])) {
                return false;
            }

            return true;
        }
    }

    public function isRequiredAction($action){
        return $this->checkPermission($action, 'require');
    }



    public function getPermission(){
        return $this->permission;
    }






    protected function setMultiUserSitePermission($role){
        // zabrani update i delete na razini svih korisničkih unosa
        $this->deny($role, array(self::UPDATE, self::DELETE));

        // omohući samo izmjenu podataka koje je korisnik insertao u bazu
        $this->allow($role, array(self::READ_USER_ENTITY, self::UPDATE_USER_ENTITY, self::DELETE_USER_ENTITY));
    }
    
    
    protected function setCatalogMultiUserSitePermission($role){
        // zabrani update i delete na razini svih korisničkih unosa
        $this->deny($role, array(self::UPDATE, self::DELETE));

        // omohući samo izmjenu podataka koje je korisnik insertao u bazu
        $this->allow($role, array(self::UPDATE_USER_ENTITY, self::DELETE_USER_ENTITY));
    }





    private function setRole($role){
        $this->permission['user_role'] = $role;
    }

    private function getRole(){
        return $this->permission['user_role'];
    }

    private function setUserId($id){
        $this->permission['user_id'] = $id;
    }

    private function getUserId(){
        return $this->permission['user_id'];
    }

    private function checkPermission($action, $permissionType){

        if (array_key_exists($permissionType, $this->permission)) {
            return true;
        }

        if (array_key_exists($this->getRole(), $this->permission[$permissionType])) {
            return true;
        }

        if (in_array($action, $this->permission[$permissionType][$this->getRole()])) {
            return true;
        }

        return false;
    }


}