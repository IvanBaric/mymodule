<?php
namespace MyModule\Entity;

class MyEntity {
	
	
//	public function hydrate(Array $options = array()){
//		$hydrator = new \Zend\Stdlib\Hydrator\ClassMethods();
//		$hydrator->hydrate($options, $this);
//	}
//
//	public function toArray(){
//		$hydrator = new \Zend\Stdlib\Hydrator\ClassMethods();
//		return $hydrator->extract($this);
//	}
	
	public function __set($key, $value) {
		$this->$key = $value;
	}
	

	public function __get($key) {
		return $this->$key;
	}
	
	public function setData($data) {
		foreach($data as $key => $value) {
			$this->__set($key, $value);
		}
	}
		
	
	public function exchangeArray($data){
		$this->setData($data);
	}
	
	
	public function getArrayCopy(){
		return $data['data'] =get_object_vars($this);
	}
	
	
	public function getEntityVars(){
		$properties = get_object_vars($this);
		$keyValue = array();
		
		foreach($properties as $key=>$value){
			array_push($keyValue, $key);
		}
		return $keyValue;
	}


	public function createSlug($str){
		$search  = array('ć', 'č', 'ž', 'đ', 'Ć', 'Č', 'Ž', 'Đ', 'š', 'Š');
		$replace = array('c', 'c', 'z', 'd', 'c', 'c', 'z', 'd', 's', 's');

		$str = str_replace($search, $replace, $str);
		$str = strtolower(trim($str));
		$str = htmlentities($str);

		$str = preg_replace('/[^a-z0-9-]/', '-', $str);
		$str = preg_replace('/-+/', "-", $str);

		return $str;
	}

} // end MyEntity








// 	public function __toString() {
// 		return json_encode($this->toArray());
// 	}


// 	public function setPassword($clear_password){
// 		$this->password = md5($clear_password);
// 	}

// public function toArray() {
// 	$dados = get_object_vars($this);
// 	foreach ($dados as $k => $item) {
// 		if (is_object($item) && isset($item->id)) {
// 			$dados[$k] = $item->id;
// 		} else {
// 			$dados[$k] = $item;
// 		}
// 	}
// 	return $dados;
// }