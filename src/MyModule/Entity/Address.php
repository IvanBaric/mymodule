<?php
namespace MyModule\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_address", options={"collate"="utf8_general_ci", "charset"="utf8"});
 */
class Address
{
    /**
     * @ORM\Column(name="id",type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $address;


    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $city;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $zipcode;


    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $company;




    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     */
    protected $user;

    public function setUser(\MyModule\Entity\User $user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }


    
    






}