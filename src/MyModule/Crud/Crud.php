<?php
namespace MyModule\Crud;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Zend\ServiceManager\ServiceManager as ServiceManager;

class Crud {

	protected $serviceManager;

	public function __construct(ServiceManager $serviceManager) {
		$this->serviceManager = $serviceManager;
		$this->setEntityManager();
	}

	protected $_em;


	public function insert($data){
		$this->checkProperties($data);

		$entity = $this->hydrate($data);

		$this->_em->persist($entity);
		$this->_em->flush();

	    return $entity;
	}

	public function edit($data){
		$entity = $this->hydrate($data);

		$this->_em->persist($entity);
		$this->_em->flush();

		return $entity;
	}

	//TODO: izbrisati insert i edit, koristiti save
	public function save($data){
		$this->checkProperties($data);

		$entity = $this->hydrate($data);

		$this->_em->persist($entity);
		$this->_em->flush();

		return $entity;
	}


	public function delete($entity){
		$this->_em->remove($entity);
		$this->_em->flush();
		return $entity;
	}


	public function hydrate($data){
		$hydrator = new DoctrineHydrator($this->_em);
		return $entity = $hydrator->hydrate($data->getArrayCopy(), $data);
	}

	
	


	private function checkProperties($class){
		$this->checkMultiUserId($class);
	}
	
		
	// USER_ID
	private function checkMultiUserId($class){
		if(property_exists($class, 'user') ){
			if(!is_object($class->getUser())){
				$user = $this->_em->getRepository('User\Entity\User')->find($this->getUser()->getId());
				($user) ? $class->setUser($user) : NULL;
			}
		}
	}
	

	private function getUser(){
		$sm = $this->serviceManager;
		$auth = $sm->get('zfcuser_auth_service');
		if ($auth->hasIdentity()) {
			return $auth->getIdentity();
		}
	}

	private function setEntityManager(){
		if(!$this->_em){
			return $this->_em = $this->serviceManager->get('Doctrine\ORM\EntityManager');
		}
		return $this->_em;
	}



























//	public function isAdmin(){
//		$sm = $this->serviceManager;
//		$auth = $sm->get('zfcuser_auth_service');
//		$role = $auth->getIdentity()->getRole();
//
//		if($role >= $this->adminRole){
//			return TRUE;
//		} else {
//			return FALSE;
//		}
//
//		#return ($role >= $this->adminRole) ? TRUE : FALSE;
//	}

//	public function save($data){
//		return ( $data->getId() ) ?  $this->edit($data) :  $this->insert($data);
//	}



	//nove metode

//	public function findBy($data){
//		$entity = $this->getObjectManager()->getRepository(get_class($data))->find($data->getId());
//	}
//
//	public function findOneBy(){
//	    ;
//	}


//	public function checkIfEntityExist($entity){
//		$entity = $this->getObjectManager()->getRepository(get_class($entity))->find($entity->getId());
//
//		return ($entity) ? TRUE : FALSE;
//	}

	// http://www.jasongrimes.org/2012/01/using-doctrine-2-in-zend-framework-2/



	// za edit
//	public function findEntity($data){
//		return $entity = $this->getObjectManager()->getRepository(get_class($data))->find($data->getId());
//	}
//
//
//	// za edit
//	public function find($data){
//		 return $entity = $this->getObjectManager()->getRepository(get_class($data))->find($data->getId());
//	}



//	public function where($data){
//		$vars = $data->getArrayCopy();
//
//		foreach($vars as $key=>$value){
//			if($value != NULL){
//				($value == 'empty') ? $value = NULL : "";
//				$where[$key] = $value;
//			}
//		}
//
//		return $where;
//		}




//		public function getEntity($data, $getUserEntity=FALSE){
//		($getUserEntity) ? $this->checkMultiUserId($data) : "";
//
//
//
//		 $entity = $this->getObjectManager()->getRepository(get_class($data))->findOneBy($this->where($data));
//
//		# echo "<pre>1. getEntity"; print_r($entity);
//
//		 return $entity;
//
//
//		}



//	public function getEntities($data, $getUserEntities=FALSE){
//		($getUserEntities) ? $this->checkMultiUserId($data) : "";
//
//		return $this->getObjectManager()->getRepository(get_class($data))->findBy($this->where($data));
//	}


//	public function batchInsert($data){
//		$this->checkMultiUserId($data);
//		$entities = $this->getEntities($data);
//
//		foreach ($entities as $entity) {
//
//			$this->getObjectManager()->persist($entity);
//		}
//
//		$this->getObjectManager()->flush();
//		return true;
//	}



}