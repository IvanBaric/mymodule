<?php
namespace MyModule\Controller;
use MyModule\Controller\MyController;
use Zend\Mvc\Application;
use Zend\View\Model\ViewModel;

class ErrorController extends MyController
{

    public function indexAction(){
        $layout = $this->layout();
        $layout->setTemplate('layout/error');
        $viewModel = new ViewModel();
        return $viewModel;
    }


}