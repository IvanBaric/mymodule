<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace MyModule\Controller;

use MyModule\Model\MyModel;
use MyModule\Permission\MyPermission;
use Zend\Mvc\Application;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use MyModule\Controller\Plugin\MyClassNameCreator;
use MyModule\Controller\Plugin\SortParamsPlugin;
use Zend\View\Model\JsonModel;
use Zend\Paginator\Paginator as Paginator;
use \MyModule\Paginator\DoctrinePaginatorAdapter as DoctrinePaginatorAdapter;
// https://github.com/doctrine/DoctrineModule/blob/master/docs/hydrator.md
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use MyModule\Hashids;
class MyController extends AbstractActionController
{
	public $table;
	public $tableObj;
	public $settings;
	public $em;
	public $permission;


	public function __construct(){
	    $this->settings = $this->getSettings();
	}


	public function indexAction(){

		$this->permission = $this->getPermission();
	    #$repository = $this->getRepository();

		return new ViewModel(array('route' => $this->settings->route, 'message' => $this->settings->message));
	}

	public function index2Action(){
//TODO: dodati permission u index2
		$form = $this->getForm();


		if ($this->getRequest()->isPost()) {
			$crud = $this->getCrud();
			$entity = $this->getEntity();
			$repository = $this->getRepository();

			$form->bind($entity);

			$form->setData($this->getRequest()->getPost());

			if (!$form->isValid()) {
				return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));
			}

			if($this->settings->recordExists) {
				if ($repository->recordExists($entity)) {
					$this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
					return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));
				}
			}

			if($crud->save($entity)){
				$this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['insert']['success']);
				return $this->redirect()->toRoute($this->settings->redirect['insert']['success'], array('action' =>'edit'));
			} else {
				return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
			}
		}

		return new ViewModel(array('form' => $form, 'route' => $this->settings->route, 'message' => $this->settings->message));

	}


/************************************************************************************************
 BATCH INSERT
*************************************************************************************************/

	public function batchInsertPrepairePostStringToArray($post){

		$po = explode(",", $post);

		$result = array();
		foreach ($po as $key => $value) {
			$r = str_replace("," , "", trim($value));
			array_push($result, $r);
		}
		#echo "<pre>"; print_r($result); echo "</pre>";die();
		return $result;
	}



/************************************************************************************************
 EDIT ACTION
*************************************************************************************************/
	public function editAction(){
		$this->permission = $this->getPermission();

		

		$id = $this->params()->fromRoute('id');
		if($id){$id = $this->decodeHash($id);}

		$table = $this->getTable();
		$form = $this->getForm();
		$entity = $this->getEntity();
		$repository = $this->getRepository();
		$crud = $this->getCrud();


		$form->bind($entity);


		// Process
		if ($this->getRequest()->isPost()) {
			$postData = $this->getRequest()->getPost();
			$form->setData($postData);

			if (!$form->isValid()) {
				return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['insert']));
			}




		// POST - edit
		if($id){
			$status = "edit";
			$entity->setId($id);

			$dbEntity = $this->getRepository()->find($id);


			if(!$dbEntity){
				die("ne postoji entity");
			}


			if (is_callable(array($table, 'save'))) {
				$entity = $table->save($entity, $dbEntity);
			}

			// TODO: dva slučaja: prvi - ne mijenja se entity ime, drugi: entity mijenja ime i tu može doći do recordExist
			if($this->settings->recordExists) {
				if ($repository->recordExists($entity)) {
					$this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
					return $this->redirect()->toRoute($this->settings->redirect['edit']['success'], array('action' =>'edit', 'id'=> $id));

				}
			}
			

			if($newData = $crud->save($entity)){
				$this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['edit']['success']);
				return $this->redirect()->toRoute($this->settings->redirect['edit']['success'], array('action' =>'edit', 'id'=> $id));
			} else {
				return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
			}
			

			
		}
		// POST -insert
		else {
			$status = "insert";

			if (is_callable(array($table, 'save'))) {
				$entity = $table->save($entity);
			}


			if($this->settings->recordExists) {
				if ($repository->recordExists($entity)) {
					$this->flashMessenger()->setNamespace('info')->addMessage($this->settings->message['insert']['recordExists']);
					return $this->redirect()->toRoute($this->settings->redirect['insert']['success'], array('action' =>'edit', 'id'=> $id));
				}
			}


			if($crud->save($entity)){
				$this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['insert']['success']);
				return $this->redirect()->toRoute($this->settings->redirect['insert']['success'], array('action' =>'edit'));
			} else {
				return $this->redirect()->toRoute($this->settings->redirect['edit']['error']);
			}
		}


		} // end if->post

		//ako nije post
		else {

		
				// GET - edit - ucitaj podatke iz baze u formu
				if($id){
					$status = "edit";

					if(!$this->permission->isAllowed(MyPermission::UPDATE) && !$this->permission->isAllowed(MyPermission::UPDATE_USER_ENTITY)){
						$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['edit']['readOnly']);
						return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
					}
					
					//TODO: napisati poruku ako: postoji entitet, a korisnik nije njegov vlasnik
					//TODO: da se razlikuje kada postoji entitet s različitim vlasnikom i kada ne postoji u bazi traženi entitet

					if($this->permission->isAllowed(MyPermission::UPDATE_USER_ENTITY)){
						$entity = $this->getRepository()->findOneBy(array('id' => $id, 'user' => $this->getUser()->getId()));
					} else {
						$entity = $this->getRepository()->find($id);
					}
					
					if(!$entity){
						$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['fetch']['emptyResult']);
						return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
					}


					$form->get('submit')->setValue($this->settings->submitButtonText['edit']);
					$form->setHydrator(new DoctrineHydrator($this->getEntityManager(), false));
					$form->bind($entity);

					return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['edit'], 'entity' => $entity, 'status' => $status, 'permission' => $this->permission->getPermission()));

				}

				// GET - inace je insert
				$status = "insert";




				if(!$this->permission->isAllowed(MyPermission::CREATE)){
					$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['insert']['readOnly']);
					return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
				}

				$form->get('submit')->setValue($this->settings->submitButtonText['insert']);

				return new ViewModel(array('form' => $form, 'pageHeading' => $this->settings->pageHeading['insert'], 'status' => $status));
		}

	}



/************************************************************************************************
 DETAILS ACTION
*************************************************************************************************/


	public function detailsAction(){
		$id = $this->params()->fromRoute('id');
		$table = $this->getTable();

		$detailsField = array($table->detailsField => $id);

		if(!$table->checkEntityId($id)){
			$this->flashMessenger()->setNamespace('error')->addMessage($table->message['fetch']['errorId']);
			return  $this->redirect()->toRoute($table->redirect['insert']['success']);
		}

		return $view = new ViewModel(array('paginator' => $table->fetchOneById($id), 'viewModelSettings' => $table->getModelSettings(), 'detailsField' => json_encode($detailsField)));
	}
/************************************************************************************************
 DELETE ACTION
*************************************************************************************************/

	public function deleteAction(){
		// deleteAction ne može se pristupiti izravnim upisom adrese u preglednik
		$referer = $this->getRequest()->getHeader('Referer');

		//TODO: napisati poruku da se izravnim pristupom preko adrese u pregledniku ne može pristupiti delete akciji
		if(!$referer) {return $this->redirect()->toRoute('error');}

		$this->permission = $this->getPermission();

		if(!$this->permission->isAllowed(MyPermission::DELETE) && !$this->permission->isAllowed(MyPermission::DELETE_USER_ENTITY)  ){
			$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['delete']['readOnly']);
			return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
		}


		$id = $this->params()->fromRoute('id'); //114
		if(!$id) {return $this->redirect()->toRoute('error');};
		$id = $this->decodeHash($id);

		if($this->permission->isAllowed(MyPermission::DELETE_USER_ENTITY)){
			$entity = $this->getRepository()->findOneBy(array('id' => $id, 'user' => $this->getUser()->getId()));
		} else {
			$entity = $this->getRepository()->find($id);
		}

		if(!$entity){
			$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['fetch']['emptyResult']);
			return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
		}


		if($this->hasOrphans($entity, $this->settings)){
			$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['delete']['deleteOrphans']);

			if($this->settings->afterDeleteRedirectToPreviousPage == TRUE){
				$referer = $this->getRequest()->getHeader('Referer');
				return ($referer) ? $this->redirect()->toUrl($referer->getUri()) : $this->redirectToErrorPage();
			} else {
				return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
			}
		}

		$crud = $this->getCrud();

		if($this->permission->isRequiredAction(MyPermission::SOFT_DELETE)){
			if (is_callable(array($entity, 'setIsDeleted'))) {
				$entity->setIsDeleted(1);
				$entity = $crud->save($entity);
				if($entity){$this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['delete']['softDelete']);}
			} else {
				$this->flashMessenger()->setNamespace('error')->addMessage($this->settings->message['delete']['softDeleteError']);
				return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
			}

		} else {
			$entity = $crud->delete($entity);
			if($entity){$this->flashMessenger()->setNamespace('success')->addMessage($this->settings->message['delete']['success']);}
		}


		if($entity){
			if($this->settings->afterDeleteRedirectToPreviousPage == TRUE){
				$referer = $this->getRequest()->getHeader('Referer');
				return ($referer) ? $this->redirect()->toUrl($referer->getUri()) : $this->redirectToErrorPage();
			} else {
				return $this->redirect()->toRoute($this->settings->redirect['delete']['success']);
			}

		} else {
			return $this->redirect()->toRoute($this->settings->redirect['delete']['error']);
		}

	}

	public function hasOrphans($entity, $settings){
		$id = $entity->getId();
		$repository = $this->getRepository();

		if($settings->hasOrphans == true){
			return $repository->hasOrphans($id, $settings);
		}

	}





/************************************************************************************************
 AJAX SEARCH ACTION
*************************************************************************************************/

	public function ajaxGETSearchAction(){
		$table = $this->getTable();

		$ajaxSearchParams = $this->checkAjaxGETSearchParams($table);
		$sortParams = $this->checkSortParams();

		$paginator = $table->fetchAll($sortParams, $ajaxSearchParams);

		return $result = new JsonModel (array('paginator' => $paginator));
	}


	public function ajaxSearchAction(){
		$this->permission = $this->getPermission();

		$repository = $this->getRepository();

		$ajaxSearchParams = $this->checkAjaxSearchParams();
		$sortParams = $this->checkSortParams();

		if($this->permission->isAllowed(MyPermission::READ_USER_ENTITY)){
			$sortParams[MyPermission::READ_USER_ENTITY] = $this->getUser()->getId();
		}

		if($this->getUser()){
			$sortParams['user_id'] = $this->getUser()->getId();
			$sortParams['user_role'] = $this->getUser()->getRole()->getAbbrv();
		}

		$results = $repository->fetchAll($sortParams);
		$paginator = $this->zendPaginator($results, $sortParams);

		return new JsonModel (array('paginator' => $paginator, 'sortParams' => $sortParams, 'route' => $this->settings->route, 'message' => $this->settings->message, 'permission' => $this->permission->getPermission()));
	}


	protected function zendPaginator($results, $sortParams){
		$adapter = new DoctrinePaginatorAdapter($results);

		$paginator = new Paginator($adapter);
		$paginator->setCurrentPageNumber($sortParams['page']);
		$paginator->setItemCountPerPage($sortParams['ipp']);
		return $paginator;
	}

/************************************************************************************************
 POMOĆNE METODE
*************************************************************************************************/
	// TODO: preurediti metodu da se koristi varijabla, a ne svaki put instanca iz service locatora
 	public function getTable(){

 		if(!isset($this->tableObj)){
 			if(!empty($this->table)){
 				return $this->tableObj = $this->serviceLocator->get($this->table);
 			} else {
 				$name = MyClassNameCreator::modelNameCreator($this);
				return (class_exists($name))? new $name($this->getEntityManager()) : new \MyModule\Model\MyModel($this->getEntityManager());
 			}
 		}
 			return $this->tableObj;
	}




	public function getForm(){
		if(!empty($this->settings->class['form'])){
			return $this->getServiceLocator()->get($this->settings->class['form']);
 		} else {
			$name = MyClassNameCreator::formNameCreator($this);
 			return new $name($this->getEntityManager());
 		}
	}

	public function getSettings(){
		if(!empty($this->settings->class['settings'])){
			return $this->getServiceLocator()->get($this->settings->class['settings']);
		} else {
			$name = MyClassNameCreator::settingsNameCreator($this);

			return (class_exists($name))? new $name : new \MyModule\Settings\MySettings();
		}
	}

 	public function getEntity(){
 		if(!empty($this->settings->class['entity'])){
 			return $this->getServiceLocator()->get($this->settings->class['entity']);
 		} else {
			$entityName = MyClassNameCreator::entityNameCreator($this);

			return new $entityName();
 		}
 	}

	public function getRepository(){
		if(!empty($this->settings->class['entity'])){
			return $this->getServiceLocator()->get($this->settings->class['entity']);
		} else {
			$entityName = MyClassNameCreator::entityNameCreator($this);
			$repository = $this->getEntityManager()->getRepository($entityName);
			return $repository;
		}
	}


	public function getPermission(){
			$name = MyClassNameCreator::permissionNameCreator($this);
			return (class_exists($name))? new $name($this->getUser()) : new \MyModule\Permission\MyPermission($this->getUser());
	}


 	public function getCrud(){
 		return $this->serviceLocator->get('MyCrud');
 	}


	public function decodeHash($id){
		$config = $this->serviceLocator->get('config');
		$hashConfig = $config['hash_ids'];
		$isHashed = $hashConfig['is_hashed'];

		if($isHashed){
			$hashids = new Hashids\Hashids($hashConfig['config']['unique_salt_value'], $hashConfig['config']['minimum_id_length'], $hashConfig['config']['custom_alphabet']);
			
			$numberArray = $hashids->decode($id);

			if(is_array($numberArray) && array_key_exists(0, $numberArray)){
				return (int)$numberArray[0];
			}

			$this->redirect()->toRoute('error');
		}

		return $id;
	}



/************************************************************************************************
AJAX SEARCH
*************************************************************************************************/
	public function checkAjaxSearchParams(){
		$post =  $this->getRequest()->getPost();

		$config = $this->AjaxSearchParamsPlugin();

		return $config = $config->getConfig($post);
	}


	public function checkAjaxGETSearchParams(){
		$post =  $this->getRequest()->getQuery();

		#echo "<pre>";print_r($post);echo "</pre>";

		$entity = $this->getEntity();
		$allowedFields = $entity->getEntityVars();

		#echo "<pre>";print_r($allowedFields);echo "</pre>";

		$config = $this->AjaxSearchParamsPlugin();

		return $config = $config->getConfig($post, $allowedFields);
	}

/************************************************************************************************
SORT
*************************************************************************************************/
	public function checkSortParams(){
		$post =  $this->getRequest()->getPost();
		$config = $this->SortParamsPlugin();
		return $config = $config->getConfig($post, $this->settings );
	}

	public function getEntityManager(){
	    return $this->serviceLocator->get('Doctrine\ORM\EntityManager');
	}


/************************************************************************************************
ERROR
 *************************************************************************************************/

	public function error(){
		$viewModel = new ViewModel();
		$viewModel->setTemplate('error/index.phtml');

		return $viewModel;
	}

	protected function redirectToErrorPageIfNotExist($param){
		if(!$param) {
			return $this->redirect()->toRoute('error');
		}
	}

	protected function redirectToErrorPage(){
		return $this->redirect()->toRoute('error');
	}


	public function getRole(){
		if ($this->zfcUserAuthentication()->hasIdentity()) {
			return  $this->zfcUserAuthentication()->getIdentity()->getRole()->getId();
		}
	}

	public function getUser(){
		if ($this->zfcUserAuthentication()->hasIdentity()) {
			return  $this->zfcUserAuthentication()->getIdentity();
		}
	}

	public function isLoggedIn(){
		return ($this->zfcUserAuthentication()->hasIdentity()) ? true : false;
	}


	
} // end MyController
