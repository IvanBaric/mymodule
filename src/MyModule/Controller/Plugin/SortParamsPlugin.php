<?php
namespace MyModule\Controller\Plugin;
 
use Zend\Mvc\Controller\Plugin\AbstractPlugin;


class SortParamsPlugin extends AbstractPlugin {

	const SHOW_DUMP = false;

	public function getConfig($post, $settings, $customSettings=false){
		$post = $post->getArrayCopy();

		$sortParams = array();



		$sortParams['page'] = isset($post['page']) && is_numeric($post['page']) ? $post['page'] : 1;
		$sortParams['ipp'] =  isset($post['ipp']) && is_numeric($post['ipp']) ? $post['ipp'] : $settings->defaultItemsPerPage;
		$sortParams['pageRange'] = isset($post['pageRange']) && is_numeric($post['pageRange']) ? $post['pageRange'] : $settings->defaultPageRange;
		$sortParams['search'] = isset($post['search']) ? $this->cleanSearchParam($post['search']) : FALSE;

		$sortParams['category'] = isset($post['category']) ? $this->cleanSearchParam($post['category']) : FALSE;
		$sortParams['tag'] = isset($post['tag']) ? $this->cleanSearchParam($post['tag']) : FALSE;


		$route = $settings->route;

		$sortParams['route'] = $route['ajaxSearch'];

		$sortParams['paginator'] = isset($post['paginator']) ? 1 : 0;


		if(isset($post['order'])){
			$allowedOrder = array('ASC', 'DESC');

			$sortParams['order'] = (in_array($post['order'], $allowedOrder)) ? $post['order'] : $settings->defaultTableOrder;
		}
		else {
			$sortParams['order'] = $settings->defaultTableOrder;
		}


		$sortParams['order_by'] = (!empty($post['order_by'])) ? $post['order_by'] : $settings->defaultTableOrderByColumn;


		//custom settings

		if($customSettings){
			$sortParams['ipp'] =  $customSettings['ipp'];
		}


		if($sortParams['page'] == 1){
			$sortParams['offset'] = 0;
        } else {
			$sortParams['offset'] = ($sortParams['page']-1)*$sortParams['ipp'];
//			$sortParams['offset'] = ($sortParams['page']*$sortParams['ipp'])-$sortParams['ipp'];
//			$sortParams['offset'] = ((int)$sortParams['page']-1)*$sortParams['ipp'];
        }





		if(self::SHOW_DUMP){
			echo "<pre>sortParams "; print_r($sortParams); echo "</pre>";
		}

		return $sortParams;
	}



	public function cleanSearchParam($search){
		$search = strip_tags($search);
		$search = stripslashes($search);
		$search = htmlspecialchars($search);
		$search = htmlentities($search);

		return $search;
	}


}

