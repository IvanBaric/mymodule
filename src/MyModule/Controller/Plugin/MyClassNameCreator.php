<?php
namespace MyModule\Controller\Plugin;
 
use Zend\Mvc\Controller\Plugin\AbstractPlugin;


class MyClassNameCreator extends AbstractPlugin{

	private static $sm;
	private static $controller_suffix = "Controller"; // AuthorController
	private static $model_suffix = "Table";  // AuthorTable
	private static $entity_suffix = ""; // Author
	private static $form_suffix = "Form";  // AuthorForm
	private static $settings_suffix = "Settings";  // AuthorSettings
	private static $permission_suffix = "Permission";  // AuthorSettings

    private static $controller_folder = "Controller";
	private static $model_folder = "Model";
	private static $entity_folder = "Entity";
	private static $form_folder = "Form";
	private static $settings_folder = "Settings";
	private static $permission_folder = "Permission";
	
	
	// TODO: za svaku metodu odrediti vraća li samo ime klase pr. AuthorTable ili cijeli namespace \Application\Model\AuthorTable
	private static $return_model_namespace = TRUE;  //return: AuthorTable
	private static $return_entity_namespace = TRUE;  //return: \Application\Model\AuthorTable
	private static $return_form_namespace = TRUE;
	private static $return_settings_namespace = TRUE;
	private static $return_permission_namespace = TRUE;
	
	
	// TODO: možda čak provjeriti postoji li u modelu ime entitija ili tablice, ako postoji koristi se ime iz modela
	
	
	// TODO: class creator (return class instance / service manager) 

	
	
	
	public static function modelNameCreator($obj){
		return self::suggestName($obj, self::$model_suffix, self::$model_folder, self::$return_model_namespace);
	}
	
      
    public static function entityNameCreator($obj){
		return self::suggestName($obj, self::$entity_suffix, self::$entity_folder, self::$return_entity_namespace);
    } 
    
    
    public static function formNameCreator($obj){
    	return self::suggestName($obj, self::$form_suffix, self::$form_folder, self::$return_form_namespace);
    }

	public static function settingsNameCreator($obj){
		return self::suggestName($obj, self::$settings_suffix, self::$settings_folder, self::$return_settings_namespace);
	}

	public static function permissionNameCreator($obj){
		return self::suggestName($obj, self::$permission_suffix, self::$permission_folder, self::$return_permission_namespace);
	}


    

    
    
    
    private static function suggestName($obj, $suffix, $folder, $namespace){
    	$explode = explode("\\", get_class($obj) );
    	 
    	if( $explode[1] == self::$controller_folder ){
    		$explode[2] = str_replace(self::$controller_suffix, $suffix, $explode[2]);
    		 
    	}
    	 
    	if( $explode[1] == self::$model_folder ){
    		$explode[2] = str_replace(self::$model_suffix, $suffix, $explode[2]);
    	}
    	 
    	$explode[1] = $folder;
    	 
    	if($namespace == TRUE){
    		$implode = implode("\\", $explode);
    		$class = "\\". $implode;
    		return $class;
    		

    		
    	}
    	else {
    		return $explode[2];

    	}
    	 
    }
   
 
}



 
