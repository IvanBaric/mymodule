<?php
namespace MyModule\Controller\Plugin;
 
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class AjaxSearchParamsPlugin extends AbstractPlugin{

	public function getConfig($post){
		$post = $post->getArrayCopy();
	
		$params = array();
		
		foreach ($post as $key => $value){
			
			$key = strip_tags(stripcslashes(trim($key)));
			$value = strip_tags(stripcslashes(trim($value)));
			


					$params[$key] = $value;


		}
		
//		echo "<pre>ajaySearchParams "; print_r($params); echo "</pre>";
		
		return $params; 
	}
}