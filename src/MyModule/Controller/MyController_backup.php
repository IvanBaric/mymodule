<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace MyModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use MyModule\Controller\Plugin\MyClassNameCreator;

class MyController extends AbstractActionController
{
	public $table;
	

	public function indexAction()
	{
		/*
			MyClassFactory::getTable();
		*/
		$form = $this->getServiceLocator()->get('CheckboxForm');
		#$table = $this->getServiceLocator()->get(MyClassNameCreator::modelNameCreator($this));
		$table = $this->getServiceLocator()->get($this->table);

		#$page = $this->page();
		#$order_by = $this->orderBy($table->defaultTableOrderByColumn);
		#$order = $this->order();
		#$search = $this->search();
		#$ipp = $this->ipp();
	
		/*
			$params[] = $this->checkParams();
				checkParams() return array
			
			$params['page'], $params['order_by'], $params['order'], $params['search'], $params['ipp'],
				
	
		$params = $this->checkParams();
		echo "<pre>";
		print_r($params);
		die();
		*/
		
		$params = $this->checkFetchAllParams();
		$paginator = $table->fetchAll($params['page'], $params['order_by'], $params['order'], $params['search'], $params['ipp']);	
		return $view = new ViewModel(array('form'=> $form, 'paginator' => $paginator, 'page' => $params['page'], 'order_by' => $params['order_by'], 'order' => $params['order'], 'search' => $params['search'], 'ipp' => $params['ipp']));
		
		
		#$paginator = $table->fetchAll($page, $order_by, $order, $search, $ipp);	
		#return $view = new ViewModel(array('form'=> $form, 'paginator' => $paginator, 'page' => $page, 'order_by' => $order_by, 'order' => $order, 'search' => $search, 'ipp' => $ipp));
	}

	
	public function editAction(){
		$table = $this->getTable();
		$form = $this->getForm();
		 

		#$crudInsert = new \MyModule\Crud\Insert\Insert();
		#$crudInsert->insert($formData = NULL);
		
		// Process
		if ($this->getRequest()->isPost()) {
			
			#$postData = $this->getRequest()->getPost();
			#$id = $this->params()->fromRoute('id');

			#$crud = new \MyModule\Crud\CrudController();
			#$crud->validateForm($form, $postData = $this->getRequest()->getPost());
			

			
			$form->setData($this->getRequest()->getPost());
			
			
			if (!$form->isValid()) {
				return new ViewModel(array('form' => $form));
			}	
				$id = $this->params()->fromRoute('id');
	
				// POST - edit
				if($id){
						
					if( $table->edit($id, $form->getData() ) ){
						$this->flashMessenger()->setNamespace('success')->addMessage($table->message['edit']['success']);
						return $this->redirect()->toRoute($table->redirect['edit']['success'], array('action' =>'edit', 'id'=> $id));
					} else {
						return $this->redirect()->toRoute($table->redirect['edit']['error']);
					}
	
	
				}
				// POST -insert
				else{
	
					if($table->insert($form->getData())){
						$this->flashMessenger()->setNamespace('success')->addMessage($table->message['insert']['success']);
						return $this->redirect()->toRoute($table->redirect['insert']['success'], array('action' =>'edit'));
					} else {
						return $this->redirect()->toRoute($table->redirect['edit']['error']);
					}
	
				}
	

	
		}
		 
		//ako nije post
		else {
	
			$id = $this->params()->fromRoute('id');
			// GET - edit - ucitaj podatke iz baze u formu
			if($id){
				$form->get('common')->get('submit')->setValue($table->submitButtonText['edit']);
				if($table->findOneBy($id)){
					$form->bind($table->findOneBy($id));
					return new ViewModel(array('form' => $form, 'pageHeading' => $table->pageHeading['edit']));
				}
				// trazi se nesto sto se ne smije
				else {
					return $this->redirect()->toRoute('djelatnici');
				}
	
			}
			// GET - inace je insert
			$form->get('common')->get('submit')->setValue($table->submitButtonText['insert']);
			return new ViewModel(array('form' => $form, 'pageHeading' => $table->pageHeading['insert']));
	
		}
	
	}
	
	public function deleteAction(){
		$table = $this->getTable();
	
		$id1 = $this->params()->fromPost('checkbox');
		$id2 = $this->params()->fromRoute('id');
		$id3 = $this->params()->fromQuery('id');
		$id4 = $this->params()->fromPost('id');
	
		if(isset($id1)){ $id = $id1; }
		if(isset($id2)){ $id = $id2; }
		if(isset($id3)){ $id = $id3; }
		if(isset($id4)){ $id = $id4; }

		
		if($table->delete($id)){
			$this->flashMessenger()->setNamespace('success')->addMessage($table->message['delete']['success']);
			return $this->redirect()->toRoute($table->redirect['delete']['success']);
		} else {
			return $this->redirect()->toRoute($table->redirect['delete']['error']);
		}
	
	}
	


	
	
	
	
	
	// pomo�ne metode
 	public function getTable(){	
 		if(!empty($this->table)){
 			return $this->getServiceLocator()->get($this->table);
 		} else {
 			return $this->getServiceLocator()->get(MyClassNameCreator::modelNameCreator($this));
 		}
	}
	

	
	
	public function getForm(){
		if(!empty($this->getTable()->class['form'])){
			return $this->getServiceLocator()->get($this->getTable()->class['form']);
 		} else {
 			return $this->getServiceLocator()->get(MyClassNameCreator::formNameCreator($this));
 		}
	}
	
 	public function getEntity(){
 		if(!empty($this->getTable()->class['entity'])){
 			return $this->getServiceLocator()->get($this->getTable()->class['entity']);
 		} else {
 			return $this->getServiceLocator()->get(MyClassNameCreator::entityNameCreator($this));
 		}
		
 	}
	
	

	
	
	public function getIdFromRoute(){
		if (!$this->idFromRoute){
			$this->idFromRoute = $this->params()->fromRoute('id');
		}
		return $this->idFromRoute;
	}
	
	
	
	public function getTableGateway(){
		if (!$this->tableGateway) {
			$this->tableGateway = $this->getServiceLocator()->get($this->tableName."Gateway");
		}
		return $this->tableGateway;
	}
	
/************************************************************************************************
SORT
*************************************************************************************************/
	
	public function checkFetchAllParams(){
		$table = $this->getServiceLocator()->get($this->table);
	
	$params['order_by'] =	$this->params()->fromRoute('order_by') ? $this->params()->fromRoute('order_by') : $table->defaultTableOrderByColumn;
	$params['order'] 	=	$this->params()->fromRoute('order') ? $this->params()->fromRoute('order') : $table->defaultTableOrder;
	$params['page']		=	$this->params()->fromRoute('page') ? $this->params()->fromRoute('page') : 1;

	if($this->params()->fromPost('search') ){
		$params['search'] = $this->params()->fromPost('search') ? $this->params()->fromPost('search') : FALSE;
	} else { 
		$params['search'] = $this->params()->fromRoute('search') ? $this->params()->fromRoute('search') : FALSE;
	}
	
	if($this->params()->fromPost('ipp')){
		$params['ipp'] = $this->params()->fromPost('ipp') ? $this->params()->fromPost('ipp') : "";
	} else {
		$params['ipp'] = $this->params()->fromRoute('ipp') ? $this->params()->fromRoute('ipp') : $table->defaultItemsPerPage;
	}
	
	return $params;
	
}
	
} // end MyController
