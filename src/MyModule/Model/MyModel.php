<?php
namespace MyModule\Model;
use MyModule\Traits\ServiceManagerTrait;

class MyModel  {
    use ServiceManagerTrait;


    public function createSlug($str){
        $search  = array('ć', 'č', 'ž', 'đ', 'Ć', 'Č', 'Ž', 'Đ', 'š', 'Š');
        $replace = array('c', 'c', 'z', 'd', 'c', 'c', 'z', 'd', 's', 's');

        $str = str_replace($search, $replace, $str);
        $str = strtolower(trim($str));
        $str = htmlentities($str);

        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);

        return $str;
    }


    /**************************************************************************************************************
     * ServiceLocator & ServiceManager
     **************************************************************************************************************/

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;

        return $this;
    }


} // end MyModel