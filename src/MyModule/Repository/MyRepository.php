<?php
namespace MyModule\Repository;
use Doctrine\ORM\EntityRepository;

class MyRepository extends EntityRepository {

    public function recordExists($entity, $dbEntity=false){
        if(!$dbEntity){
            //insert
            return $this->findOneBy(array('
            ' => $entity->getName()));
        } else {
            return ($dbEntity->getName() == $entity->getName()) ? false : true;
            // update, treba omogućiti snimanje u bazu - return false
            // upravo je naziv jednog entitija preimenovan u već postojeći entity - return true
        }
    }

    public function hasOrphans($id, $settings){
        $entity = $this->_em->getRepository($settings->hasOrphansClass)->findBy(array($settings->hasOrphansProperty => $id));
        if(count($entity)>0){
            return true;
        }
        return false;
    }


    public function getDropdown(){
        $results = $this->findAll();

        //paziti radi li se o upitu findOneBy ili findOne
        $array = array();
        foreach($results as $result){
            $id = $result->getId();
            $name = $result->getName();


            $array[$id] = $name;
        }

        return $array;
    }


    public function getLastInserted($num){
        return $entity = $this->findBy(array(), array('createdAt' => 'DESC'), '', $num);
    }


}