<?php
namespace MyModule\Form\BatchInsert;

use Zend\InputFilter\InputFilter;

class SimpleBatchInsertFilter extends InputFilter {
	public function __contruct() {

		$this->add(array(
				'name' => 'data',
				'required' => true,
				'filters' => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				),
				// 			'validators' => array(),
					
		));

	}
}