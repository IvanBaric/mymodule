<?php
namespace MyModule\Form\BatchInsert;

use Zend\Form\Form;

class SimpleBatchInsertForm extends Form {
	public function __construct($name=null) {
		parent::__construct('SimpleBatchInsert');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setHydrator(new \Zend\Stdlib\Hydrator\Reflection());



		$this->add(array(
				'name' => 'data',
				'attributes' => array(
						'type' => 'text',
						'class' => 'tokenfield',
						'style' => 'width:100%',
						'value' => 'Knj',
				),
				'options' => array(
						'id' =>'data',
						'label' => 'data',

				),
		));
		
		
		
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn btn-primary btn-small pull-right',
						'value' => 'Unesi u bazu podataka',
				),
		));

		#$this->add(new \Zend\Form\Element\Csrf('security'));
		


		#$this->add(new \Application\Form\CommonFieldset());
	}
}