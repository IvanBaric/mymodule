<?php
namespace MyModule\Form;
use Zend\Form\Form;

class CheckboxForm extends Form {

	
	public function __construct($name=NULL) {

		
		parent::__construct('Checkbox');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');

		$this->add(array(
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'select-all',
				'attributes' => array(
						'id' => 'select-all',
						'class' => 'selectedId',
				),
				'options' => array(
						'label' => 'A checkbox',
						'use_hidden_element' => false,
						#'checked_value' => 'good',
						#'unchecked_value' => 'bad'
				)
		));
		
		
		$this->add(array(
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'checkbox[]',
				'attributes' => array(
						'class' => 'checkbox1',
				),
				'options' => array(
						'label' => 'A checkbox',
						'use_hidden_element' => false,
						#'checked_value' => 'good',
						#'unchecked_value' => 'bad'
				)
		));
		
		$this->add(array(
				'name' => 'search',
				'attributes' => array(
						'type' => 'text',
						'class' => 'pull-right',
						'placeholder' => 'traži...',
						
				),
				'options' => array(
						'id' =>'search',
						'label' => 'search',
						
		
				),
		));
		
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn',
						'value' => 'Search',

		
				),
				'options' => array(
						'id' =>'submit',
						'label' => 'submit',
		
		
				),
		));
		
		
		$this->add(array(
				'type' => 'Select',
				'name' => 'ipp',
				'attributes' => array(
					'class' => 'span1'
				),
				'options' => array(
						'label' => 'Items per page',
						'value_options' => array(
								'2' =>'2',
								'5' =>'5',
								'10' =>'10',
								'25'=>'25',
								'50'=>'50',
						),
				)
		));
		
		$this->add(array(
				'type' => 'Select',
				'name' => 'akcija',
				'attributes' => array(
						'class' => 'span2'
				),
				'options' => array(
						'label' => 'Masovne akcije',
						'value_options' => array(
								'0' => 'Masovne akcije',
								'delete' =>'Obriši označeno',
						),
				)
		));
			
		$this->add(new \MyModule\Form\CommonFieldset());
		
	}
	
}