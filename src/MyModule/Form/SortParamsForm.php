<?php
namespace MyModule\Form;
use Zend\Form\Form;

class SortParamsForm extends Form {
	// SortParams: order_by, order, ipp, page, dodati još page_range
	// 	order_by: popis polja
	//	order: ASC, DESC
	// 	ipp: broj
	// 	page: broj
	// 	page_range: broj
	// TODO: napraviti objektnu validaciju params - to će mi vrijediti za cijeli projekt i sljedeće projekte
	
	public function __construct($name=NULL) {

		
		parent::__construct('SortParams');
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
		#$this->setHydrator(new \Zend\Stdlib\Hydrator\Reflection());
		#$this->bind(new \Application\Entity\Clanovi());
		
		$this->add(array(
			'name' => 'order',
			'attributes' => array(
				'type' => 'text',
			),
		));

	
		$this->add(array(
			'name' => 'order_by',
			'attributes' => array(
				'type' => 'text',
			),
		));
		
		$this->add(array(
				'name' => 'ipp',
				'attributes' => array(
						'type' => 'number',
				),
		));
		
		$this->add(array(
				'name' => 'page',
				'attributes' => array(
						'type' => 'number',
				),
		));
		
	
		
		#$this->add(new \Knjiznica\Form\MainFieldset());
		
		#$this->add(new \Application\Form\CommonFieldset());
		
	}
	
}