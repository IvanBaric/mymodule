<?php
namespace MyModule\Form;
use Zend\InputFilter\InputFilter;

class SortParamsFilter extends InputFilter {
	public function __construct()
	{


		
		

		$this->add(array(
			'name' => 'order',
			'required' => false,
			'filters' => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			)
		));
		
		
		$this->add(array(
				'name' => 'spol',
				'required' => false,
				'filters' => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				)
		));
		
		$this->add(array(
				'name' => 'biljeska',
				'required' => false,
				'filters' => array(
					#	array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				)
		));
		
		$this->add(array(
				'name' => 'broj_primjeraka',
				'required' => true,
				'filters' => array(
						#	array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				)
		));
		

		$this->add(array(
				'name' => 'id_razred',
				'required' => true,
				'filters' => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				)
		));
		
		

		$this->add(array(
				'name' => 'id_po',
				'required' => false,
				'filters' => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				)
		));

	}
}