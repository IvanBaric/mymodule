<?php

namespace MyModule\Form;

use Zend\Form\Fieldset;

class CommonFieldset extends Fieldset {
	public function __construct() {
		parent::__construct('common');
		
		$this->setAttribute('method', 'post');
		$this->setAttribute('enctype', 'multipart/form-data');
	
		$this->add(new \Zend\Form\Element\Csrf('csrf'));
		
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'class' => 'btn',
				'value' => 'submit',
			),				
		));
		
		$this->add(array(
				'name' => 'submit-mini',
				'attributes' => array(
						'type' => 'submit',
						'class' => 'btn btn-mini',
						'value' => 'submit',
				),
		));
		
		$this->add(array(
				'type' => 'Zend\Form\Element\Checkbox',
				'name' => 'checkbox[]',
				'attributes' => array(
						'class' => 'checkbox1',
				),
				'options' => array(
						'label' => 'A checkbox',
						'use_hidden_element' => false,
						#'checked_value' => 'good',
						#'unchecked_value' => 'bad'
				)
		));
		
		
		

		

		
		
		
	}
}