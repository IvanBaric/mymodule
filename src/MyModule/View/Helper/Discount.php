<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Discount extends AbstractHelper {

	public function __invoke($property) {

		if($property) {
			$output = $property . " %";
		}


		return $output;
	}
	
}