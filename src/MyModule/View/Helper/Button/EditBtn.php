<?php
namespace MyModule\View\Helper\Button;

use Zend\View\Helper\AbstractHelper;

class EditBtn extends AbstractHelper {

	public function __invoke($edit_url, $id, $text = "Uredi", $type='button') {

		// $this->view->otherHelper();
		// ili
		// $this->view->plugin('viewhelpername')

		$buttonClass = "btn btn-warning";
		$iconClass = "fa fa-pencil-square-o";
		$urlHelper = $this->view->plugin('url');

		$urlHelper = $urlHelper($edit_url, array('action'=>'edit', 'id'=>$id));



		if($type == "button") {
			$output = "<a href='{$urlHelper}' class='{$buttonClass}'> <i class='{$iconClass}' aria-hidden='true'></i> {$text}</a>";
		}

		if($type == "text") {
			$output = "<a href='{$urlHelper}'>{$text}</a>";
		}

		if($type == "icon"){
			$output = "<a href='{$urlHelper}' class='{$buttonClass}'> <i class='{$iconClass}' aria-hidden='true'></i></a>";
		}

		return $output;
	}
	
}