<?php
namespace MyModule\View\Helper\Button;

use Zend\View\Helper\AbstractHelper;

class BtnGroup extends AbstractHelper {

	public function __invoke($array) {

		$output = "<div class='btn-group'><a class='btn btn-mini dropdown-toggle' data-toggle='dropdown' href='#'>P<span class='caret'></span></a><ul class='dropdown-menu'>";
    	
		foreach ($array as $key => $value) {
			$output .= "<li><a href='{$key}'>{$value}</a></li>";
		}
    
		$output .= "</ul></div>";
		
		return $output;
	}
	
}

