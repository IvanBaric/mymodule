<?php
namespace MyModule\View\Helper\Button;

use Zend\View\Helper\AbstractHelper;

class DeleteBtn extends AbstractHelper {

	public function __invoke($delete_url, $id=false, $text=false, $orphanCheck=false, $orphanErrorMsg="", $showOnlyImage=false) {

		if($showOnlyImage){
			$text = "";
		} else {
			$text = (!empty($text)) ? $text : "Izbriši";
		}


		$orphanErrorMsg = $orphanErrorMsg['delete']['deleteOrphans'];

		$urlHelper = $this->view->plugin('url');

		$urlHelper = $urlHelper($delete_url, array('action'=>'delete', 'id'=>$id));


		//ima unosa u drugoj tablici
		if($orphanCheck > 0) {

			$output = "<a href='#' class='btn btn-danger orphanError' data-message='{$orphanErrorMsg}'><i class='fa fa-times' aria-hidden='true'></i> {$text}</a>";

		} else {

			$deleteUrl = $delete_url."/".$id;

			$output = "<a href='{$urlHelper}' class='btn btn-danger delete'><i class='fa fa-times' aria-hidden='true'></i> {$text}</a>";

		}

		return $output;
	}
	
}


