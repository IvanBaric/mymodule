<?php
namespace MyModule\View\Helper\Hashids;

use Zend\View\Helper\AbstractHelper;
use MyModule\Hashids;

class Hash extends AbstractHelper {


    public function __invoke($id, $config=false) {
        $isHashed = $config['is_hashed'];

        if($isHashed){
            $config = $config['config'];
            $salt = $config['unique_salt_value'];
            $length = $config['minimum_id_length'];
            $customAlphabet = $config['custom_alphabet'];

            $hashids = new Hashids\Hashids($salt, $length, $customAlphabet);
            return $id = $hashids->encode($id);
        }

		return $id;
	}
	
}
?>
<!--<div class='ribbon-wrapper'><div class='ribbon default'>Admin</div></div>-->