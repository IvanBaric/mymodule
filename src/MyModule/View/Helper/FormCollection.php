<?php
namespace MyModule\View\Helper;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormCollection as BaseFormCollection;
use Zend\View\Helper\AbstractHelper;

class FormCollection extends BaseFormCollection {
	public function render(ElementInterface $element) {
		return '<ul>'.parent::render($element).'</ul>';
	}
}