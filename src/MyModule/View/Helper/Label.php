<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Label extends AbstractHelper {

	public function __invoke($type='info', $text) {

		$output = "<span class='label label-{$type}'>{$text}</span>";
		
		return $output;
	}
	
}