<?php
namespace MyModule\View\Helper\Box;

use Zend\View\Helper\AbstractHelper;

class BoxGeneric extends AbstractHelper {

	public function __invoke($msg="BoxGeneric") {

		$output = "<div class='box-generic'>";
		$output .= $msg;
		$output .= "</div>";
		
		return $output;
	}
	
}
?>
<!--<div class='ribbon-wrapper'><div class='ribbon default'>Admin</div></div>-->