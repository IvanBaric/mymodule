<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class IsFeatured extends AbstractHelper {

	public function __invoke($property) {




		if($property == 1) {
			$output = "<i class='fa fa-plus-circle' aria-hidden=\"true\"></i>";
		} else {
			$output = "<i class='fa fa-minus-circle' aria-hidden=\"true\"></i>";
		}


		return $output;
	}
	
}