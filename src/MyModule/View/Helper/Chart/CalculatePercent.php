<?php
namespace MyModule\View\Helper\Chart;

use Zend\View\Helper\AbstractHelper;

class CalculatePercent extends AbstractHelper {

	public function __invoke($query) {
	
		$ukupnoDjela = "";
		
		foreach ($query as $e){
			$ukupnoDjela += $e['data'];
		
		}
		
		$rezultat = array();
		
		foreach ($query as $e){
			$postotak = round(($e['data']/$ukupnoDjela)*100);
			$rezultat[] = array('label' => $e['label'], 'data' => $postotak);
		}

		return $rezultat;
	}
	
}