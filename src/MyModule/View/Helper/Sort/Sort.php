<?php
namespace MyModule\View\Helper\Sort;

use Zend\View\Helper\AbstractHelper;

class Sort extends AbstractHelper {

	const SHOW_DUMP = false;

	public function __invoke($naziv_polja_u_bazi, $naziv_polja, $config) {

		if(!$config['paginator']) {
			$config['order'] = ($config['order'] == "ASC") ? "DESC" : "ASC";
		}

		$config['ikona'] = ($config['order'] == "ASC") ? "-desc" : "-asc";

		#$urlHelper = $this->view->plugin('url');
		
		$ikona = ($config['order_by'] == $naziv_polja_u_bazi) ? $config['ikona'] : "";

		if(self::SHOW_DUMP) {
			echo "<pre>";
			print_r($config);
			echo "</pre>";
		}

		$urlHelper = $this->view->plugin('url');

		$urlHelper =  $urlHelper($config['route']);
			
		#$url = $urlHelper($config['paginator_routa'], array('page'=> $config['page'] , 'order_by' => $naziv_polja_u_bazi, 'order' => $config['order'] , 'ipp' => $config['ipp']));
	
		#$output = "<a class='ajaxSort' data-page={$config['page']} data-order_by={$naziv_polja_u_bazi} data-order={$config['order']} data-search={$config['search']} data-ipp={$config['ipp']} href='{$url}'>" .$naziv_polja. " " ."<i class='icon-chevron-{$ikona}'</a>";
		
		$output = "<a class='ajaxSort'  data-page={$config['page']} data-order_by={$naziv_polja_u_bazi} data-order={$config['order']} data-search='{$config['search']}' data-route={$urlHelper}   href='#'>" .$naziv_polja. " " ."<i class='fa fa-sort{$ikona}' aria-hidden='true'></i> </a>";
		
		return $output;
	}	
}

