<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Base extends AbstractHelper {

	public function __invoke() {

		$urlHelper = $this->view->plugin('url');

		return  $urlHelper('home',array(),array('force_canonical' => true));

	}
	
}