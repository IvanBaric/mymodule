<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Price extends AbstractHelper {

	public function __invoke($property) {

		$output = "";

		if($property || $property == 0) {
			$numFormat = number_format ($property, 2);
			$output = "€".$numFormat;
		}




		return $output;
	}
	
}