<?php
namespace MyModule\View\Helper;
use Zend\Form\ElementInterface;
use Zend\Form\View\Helper\FormElement as BaseFormElement;
use Zend\View\Helper\AbstractHelper;

class FormElement extends BaseFormElement {

	public function render(ElementInterface $element) {
		if ($element->getOption('required')) {
			$req = 'required';
		} else {
			$req = "";
		}
		$type = $element->getAttribute('type');
		$name = $element->getAttribute('name');
		return sprintf('<li class="%s %s %s">%s</li>', $name, $req, $type,  parent::render($element));
	}
}


