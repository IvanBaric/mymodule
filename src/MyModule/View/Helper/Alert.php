<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Alert extends AbstractHelper {

	public function __invoke($type='info', $text) {

		$output = "<div class='alert alert-{$type} '><button type='button' class='close' data-dismiss='alert'>&times;</button>{$text}</div>";
		
		return $output;
	}
	
}