<?php
namespace MyModule\View\Helper\Action;

use Zend\View\Helper\AbstractHelper;

class Delete extends AbstractHelper {

	public function __invoke($delete_url, $id, $text=false, $orphanCheck=false, $orphanErrorMsg="", $showOnlyImage=false, $permission=false, $row=false) {

		if($permission) {

            $isAllowed = $this->view->permission(\MyModule\Permission\MyPermission::DELETE, $permission, $row);
			



            if (!$isAllowed) {
                return "";
            }
        }


		if($showOnlyImage){
			$text = "";
		} else {
			$text = (!empty($text)) ? $text : "Izbriši";
		}


		$orphanErrorMsg = $orphanErrorMsg['delete']['deleteOrphans'];

		$urlHelper = $this->view->plugin('url');

		$urlHelper = $urlHelper($delete_url, array('action'=>'delete', 'id'=>$id));


		//ima unosa u drugoj tablici
		if($orphanCheck > 0) {

			$output = "<a href='#' class='btn btn-danger orphanError' data-message='{$orphanErrorMsg}'><i class='fa fa-times' aria-hidden='true'></i> {$text}</a>";

		} else {

			$output = "<a href='{$urlHelper}' class='btn btn-danger delete'><i class='fa fa-times' aria-hidden='true'></i> {$text}</a>";

		}

		return $output;
	}
	
}


