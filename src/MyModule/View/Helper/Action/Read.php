<?php
namespace MyModule\View\Helper\Action;

use Zend\View\Helper\AbstractHelper;

class Read extends AbstractHelper {

    public function __invoke($edit_url, $id, $type='icon', $permission = false, $row, $text = "Uredi") {

        $buttonClass = "btn btn-info";
        $iconClass = "fa fa-eye";
        $urlHelper = $this->view->plugin('url');

        $urlHelper = $urlHelper($edit_url, array('action'=>'single', 'id'=>$id));


        // $this->view->plugin('viewhelpername')
        if($permission){

            $isAllowed = $this->view->permission(\MyModule\Permission\MyPermission::READ, $permission, $row);

            if(!$isAllowed && $type != "link"){
                return "";
            }

            if($type == "link" && !$isAllowed) {
                $output = $text;
            }

            if($type == "link" && $isAllowed) {
                $output = "<a href='{$urlHelper}'>{$text}</a>";
            }

        }



        if($type == "button") {
            $output = "<a href='{$urlHelper}' class='{$buttonClass}'> <i class='{$iconClass}' aria-hidden='true'></i> {$text}</a>";
        }

        if($type == "text") {
            $output = "<a href='{$urlHelper}'>{$text}</a>";
        }

        if($type == "icon"){
            $output = "<a href='{$urlHelper}' class='{$buttonClass}'> <i class='{$iconClass}' aria-hidden='true'></i></a>";
        }


        return $output;
    }

}