<?php
namespace MyModule\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Tax extends AbstractHelper {

	public function __invoke($property, $tax=false) {

		if(!$tax){
			$tax = 0.2;
		}

		$output = "";

		if($property) {
			$tax = $property * $tax;
			$numFormat = number_format($tax, 2);
			$output = "€".$numFormat;
		}


		return $output;
	}
	
}