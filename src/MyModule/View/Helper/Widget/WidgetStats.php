<?php
namespace MyModule\View\Helper\Widget;

use Zend\View\Helper\AbstractHelper;

class WidgetStats extends AbstractHelper {

	public function __invoke($icon='user_add', $text, $number) {
	
		$output = "<a href='' class='widget-stats'>
		<span class='glyphicons {$icon}'><i></i></span>
		<span class='txt'>{$text}</span>
		<div class='clearfix'></div>
		<span class='count label label-primary'>{$number}</span></a><br />";
		
		return $output;
	}
	
}