<?php
namespace MyModule\View\Helper\Permission;

use MyModule\Permission\MyPermission;
use Zend\View\Helper\AbstractHelper;

class Permission extends AbstractHelper {

	public function __invoke($globalAction, $permission, $row)
    {
        
//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($permission); die();

        // ako nije ništa definirano (allow i deny) u klasi, znači da je sve dopušteno korisniku
        if (!array_key_exists('deny', $permission) && !array_key_exists('allow', $permission)) {
            return true;
        }


        $isAllowed = false;
        $isUserActionAllowed = false;

        $role = $permission['user_role'];
        
        
//        echo "<pre>";
//        \Doctrine\Common\Util\Debug::dump($permission); die();


        $userAction = ($globalAction == MyPermission::UPDATE) ? MyPermission::UPDATE_USER_ENTITY : MyPermission::DELETE_USER_ENTITY;


        // ako ne postoji nikakav deny, akcija je dopuštena
        if (!array_key_exists('deny', $permission)) {
            $isAllowed = true;
        } else {
            // ako ne postoji nikakav deny, za role, akcija je dopuštena
            if (!array_key_exists($role, $permission['deny'])) {
                $isAllowed = true;
            } else {
                // deny array postoji, ali nema u njemu akcija kojoj želimo pristupiti
                if (!in_array($globalAction, $permission['deny'][$role])) {
                    $isAllowed = true;
                }
            }




        }


        // ako ne postoji nikakav deny, akcija je dopuštena
        if (!array_key_exists('allow', $permission)) {

            $isUserActionAllowed = false;
        } else {

            // ako ne postoji nikakav deny, za role, akcija je dopuštena
            if (!array_key_exists($role, $permission['allow'])) {
                $isUserActionAllowed = false;
            }


            // deny array postoji, ali nema u njemu akcija kojoj želimo pristupiti
            if (!in_array($userAction, $permission['allow'][$role])) {
                $isUserActionAllowed = false;

            } else {
                $isUserActionAllowed = true;
            }


        }


        if ($isUserActionAllowed) {
            if (!$row->getUser()) {die("entitet nije povezan s user modelom");}

            return ($permission['user_id'] == $row->getUser()->getId()) ? true : false;

        } else {
            return ($isAllowed) ? true : false;
        }



    }
}