<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

class FlashMessages extends AbstractHelper
{

    public function __invoke()
    {
    	
    	
    	$namespaces = array ('error','success','info','default');
	    	foreach ($namespaces as $namespace) {
	    		$flashMessages =  $this->flashmessenger()->render($namespace);
	    		if (!empty($flashMessages)) {
			    	$output  = "";
			    	$output .= "<div id='myAlert'' class='alert alert-{$namespace}' data-alert='alert'>";
			    	$output .= "<a class='close' data-dismiss='alert'>x</a>";
			    	$output .= $flashMessages;
			
	    			return $output;
	    	}
   		}
   		
   		
   		


    }

}