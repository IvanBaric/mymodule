<?php
namespace MyModule\View\Helper\Table;

use Zend\View\Helper\AbstractHelper;

class TableOpenTag extends AbstractHelper {

	public function __invoke($id = 'mytable', $class='table') {

		$output = "<table id='{$id}' class='{$class}'>";
		
		return $output;
	}
	
}