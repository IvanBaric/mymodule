<?php
namespace MyModule\View\Helper\Table;

use Zend\View\Helper\AbstractHelper;

class checkFetchError extends AbstractHelper {

	public function __invoke($paginator, $text=FALSE) {

		if(!$text){
			$text = "Nema rezultata.";
		}
		
		$output = "<div class='alert alert-info '><button type='button' class='close' data-dismiss='alert'>&times;</button><i class='fa fa-info-circle' aria-hidden='true'></i> {$text}</div>";
		
		if(!count($paginator)>0){
			die($output);
		}
	}
	
}