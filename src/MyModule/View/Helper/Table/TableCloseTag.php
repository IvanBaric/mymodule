<?php
namespace MyModule\View\Helper\Table;

use Zend\View\Helper\AbstractHelper;

class TableCloseTag extends AbstractHelper {

	public function __invoke() {

		$output = "</table>";
		
		return $output;
	}
	
}