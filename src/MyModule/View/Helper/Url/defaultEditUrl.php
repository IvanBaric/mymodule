<?php
namespace MyModule\View\Helper\Url;

use Zend\View\Helper\AbstractHelper;

class EditDeleteUrl extends AbstractHelper {

	public function __invoke($editRoute, $id) {

		$urlHelper = $this->view->plugin('url');
		
		$urlHelper = $urlHelper($editRoute, array('action'=>'edit', 'id'=>$id));
		
		return $urlHelper;
	}
	
}