<?php
namespace MyModule\View\Helper\Url;

use Zend\View\Helper\AbstractHelper;

class EditUrl extends AbstractHelper {

	public function __invoke($editRoute, $action, $id) {

		$urlHelper = $this->view->plugin('url');
		
		$urlHelper = (empty($action)) ? $urlHelper($editRoute, array('id'=>$id)) : $urlHelper($editRoute, array('action'=>$action, 'id'=>$id));
		
		return $urlHelper;
	}
	
}