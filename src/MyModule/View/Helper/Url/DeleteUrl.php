<?php
namespace MyModule\View\Helper\Url;

use Zend\View\Helper\AbstractHelper;

class DeleteUrl extends AbstractHelper {

	public function __invoke($deleteRoute, $action, $id) {

		$urlHelper = $this->view->plugin('url');
		
		$urlHelper = (empty($action)) ? $urlHelper($deleteRoute, array('id'=>$id)) : $urlHelper($deleteRoute, array('action'=>$action, 'id'=>$id));
		
		return $urlHelper;
	}
	
}