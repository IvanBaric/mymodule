<?php
namespace MyModule\Traits;
 
use Zend\ServiceManager\ServiceManager;
#use Zend\ServiceManager\ServiceManagerAwareInterface;
#use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


#use MyModule\Traits\ServiceManagerTrait;


 
trait ServiceManagerTrait
{
  /**
   * @var ServiceManager
   */
  protected $serviceManager;
 
  /**
   * Retrieve service manager instance
   *
   * @return ServiceManager
   */
  public function getServiceManager()
  {
      return $this->serviceManager;
  }
 
  /**
   * Set service manager instance
   *
   * @param ServiceManager $locator
   * @return User
   */
  public function setServiceManager(ServiceManager $serviceManager)
  {
      $this->serviceManager = $serviceManager;
      return $this;
  }


  protected $services;


    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
  {
    $this->services = $serviceLocator;
  }
  
  public function getServiceLocator()
  {
    return $this->services;
  }
}

/*
 Template za ServiceManager i EntityManager
 <?php
namespace Application\Service;
 
use Zend\EventManager\EventManagerAwareInterface;
use Zend\ServiceManager\ServiceManagerAwareInterface;
 
use Application\Provider\ProvidesServiceManager;
use Zend\EventManager\ProvidesEvents;
 
class MyService  implements ServiceManagerAwareInterface, EventManagerAwareInterface
{
    use ServiceManagerTrait, ProvidesEvents; //Insert Multiple Traits
 
    // implement code here....
}

Module.php

        'factories' => array(
            'Application\Crud\Crud' => function($sm){
              $model = new \Application\Crud\Crud(); 
              $model->setServiceLocator($sm);
              return $model;
            },
        ),
 
 */