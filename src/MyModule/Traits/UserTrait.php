<?php
namespace MyModule\Traits;



trait UserTrait
{
  protected $user;

  public function setUser($user){
    $this->user = $user;
  }

  public function getUser()
  {
    return $this->user;
  }

  public function getUserId(){
      return $this->user->getId();
  }

}