<?php
namespace MyModule\Traits;

interface UserTraitInterface {

    public function getUser();
    public function setUser($user);
}