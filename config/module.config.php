<?php
return array(
		'hash_ids' => array(
			'is_hashed' => false,
			'config' => array(
				'unique_salt_value' => '3A0BI5uH4CKep2hiNI1',
				'minimum_id_length' => 32,
				'custom_alphabet' => 'abcdefghijklmnoprstuvzyx1234567890'
			)
		),

		'token' => array(
				'is_hashed' => true,
				'config' => array(
						'unique_salt_value' => '3A0BI5uH4CKep2hiNI1HJ21',
						'minimum_id_length' => 16,
						'custom_alphabet' => 'abcdefghijklmnoprstuvzyx1234567890'
				)
		),


		'controllers' => array(
				'invokables' => array(
						'MyModule\Controller\MyController' => 'MyModule\Controller\MyController',
						'MyModule\Crud\CrudController ' => 'MyModule\Crud\Crud',
						'MyModule\Controller\Error' => 'MyModule\Controller\ErrorController',
				),
		),
		'controller_plugins' => array(
				'invokables' => array(
						'MyClassNameCreator' => 'MyModule\Controller\Plugin\MyClassNameCreator',
						'SortParamsPlugin' => 'MyModule\Controller\Plugin\SortParamsPlugin',
						'AjaxSearchParamsPlugin' => 'MyModule\Controller\Plugin\AjaxSearchParamsPlugin',
				)
		),
		
		'view_manager' => array(
				'template_path_stack' => array(
						__DIR__ . '/../view',
				),
				'display_exceptions'       => true,
				'exception_template'       => 'error',
		),

		'router' => array(
				'routes' => array(
						'error' => array(
								'type'    => 'Literal',
								'options' => array(
										'route'    => '/error',
										'defaults' => array(
												'__NAMESPACE__' => 'MyModule\Controller',
												'controller'    => 'Error',
												'action'        => 'index',
										),
								),
						),


				)
		)
		
//		'cache' => array(
//			'adapter' => 'Apc',
//			'ttl' => 10
//		),

		
		
		
);